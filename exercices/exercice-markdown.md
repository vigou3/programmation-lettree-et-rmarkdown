# Syntaxe Markdown

Markdown permet d'indiquer simplement de l'*italique*
ou du **gras**.

## Listes

- Épicerie
  1. Bananes
  2. Hamburger
	 * pain
	 * viande
	 
- Quincaillerie

## Hyperlien

[Site officiel de Markdown](https://daringfireball.net/projects/markdown/)

## Insertion d'une image

![](https://tinyurl.com/jszvpfj)

## Code informatique

Le code informatique (aussi appelé texte verbatim) est affiché 
exactement tel qu'il apparait dans le code source, habituellement
dans une police à largeur fixe, et parfois mis en évidence par un
encadré ou un fond de couleur contrastante.

```
x <- rgamma(10, 2)
```
