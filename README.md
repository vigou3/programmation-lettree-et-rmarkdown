<!-- Emacs: -*- coding: utf-8; eval: (auto-fill-mode -1); eval: (visual-line-mode t) -*- -->

# Programmation lettrée et R Markdown

[*Programmation lettrée et R Markdown*](https://vigou3.gitlab.io/programmation-lettree-et-rmarkdown) est un laboratoire (ou atelier) d'introduction à la programmation lettrée avec [R
Markdown](https://rmarkdown.rstudio.com) offert dans le cadre du cours [ACT-2002 Méthodes numériques en actuariat](https://www.ulaval.ca/les-etudes/cours/repertoire/detailsCours/act-2002-methodes-numeriques-en-actuariat.html) à l'[École d'actuariat](https://www.act.ulaval.ca) de l'[Université Laval](https://ulaval.ca).

Le laboratoire consiste en une présentation entrecoupée d'exemples et d'exercices.

Les fichiers nécessaires pour réaliser les exercices se trouvent dans le dossier `exercices`.

## Auteur

Vincent Goulet, professeur titulaire, École d'actuariat, Université Laval

## Licence

«Programmation lettrée et R Markdown» est dérivé de «[R & Markdown](https://github.com/davebulaval/R_Markdown)» de David Beauchemin et Samuel Lévesque, et «[Outils de travail collaboratif](https://github.com/lculaval/OTC)» de Laurent Caron, tous deux sous licence [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).

«Programmation lettrée et R Markdown» est mis à disposition sous licence [Attribution-Partage dans les mêmes conditions 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) de Creative Commons.

Consulter le fichier `LICENSE` pour la licence complète.

## Modèle de développement

Le processus de rédaction et de maintenance du projet suit le modèle [*Gitflow Workflow*](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow ). Seule particularité: la branche *master* se trouve dans le dépôt [`programmation-lettree-et-rmarkdown`](https://gitlab.com/vigou3/programmation-lettree-et-rmarkdown) dans GitLab, alors que la branche de développement se trouve dans le dépôt [`programmation-lettree-et-rmarkdown-devel`](https://projets.fsg.ulaval.ca/git/scm/vg/programmation-lettree-et-rmarkdown-devel) dans le serveur BitBucket de la Faculté des sciences et de génie de l'Université Laval.

Prière de passer par le dépôt `programmation-lettree-et-rmarkdown-devel` pour proposer des modifications; consulter le fichier `CONTRIBUTING.md` pour la marche à suivre.
