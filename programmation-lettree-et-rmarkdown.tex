%%% Copyright (C) 2018-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet «Programmation lettrée et R
%%% Markdown»
%%% https://gitlab.com/vigou3/programmation-lettree-et-rmarkdown
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0

\documentclass[aspectratio=169,10pt,xcolor=x11names,english,french]{beamer}
  \usepackage{babel}
  \usepackage[autolanguage]{numprint}
  \usepackage{amsmath}
  \usepackage[mathrm=sym]{unicode-math}  % polices math
  \usepackage{fontawesome5}              % various icons
  \usepackage{awesomebox}                % \tipbox et autres
  \usepackage{changepage}                % page licence
  \usepackage{tabularx}                  % page licence
  \usepackage{booktabs}                  % beaux tableaux
  \usepackage{listings}                  % code source
  \usepackage{fancyvrb}                  % texte verbatim
  \usepackage{framed}                    % env. leftbar
  \usepackage[overlay,absolute]{textpos} % covers
  \usepackage{metalogo}                  % logo \XeLaTeX

  %%% =============================
  %%%  Informations de publication
  %%% =============================
  \title{Programmation lettrée et R~Markdown}
  \author{Vincent Goulet}
  \renewcommand{\year}{2025}
  \renewcommand{\month}{02}
  \newcommand{\reposurl}{https://gitlab.com/vigou3/programmation-lettree-et-rmarkdown}

  %%% ===================
  %%%  Style du document
  %%% ===================

  %% Beamer theme
  \usetheme{metropolis}
  \metroset{subsectionpage=progressbar}

  %% Polices de caractères
  \setsansfont{Fira Sans Book}
  [
    BoldFont = {Fira Sans SemiBold},
    ItalicFont = {Fira Sans Book Italic},
    BoldItalicFont = {Fira Sans SemiBold Italic}
  ]
  \setmathfont{Fira Math}
  \newfontfamily\titlefontOS{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = OldStyle
  ]
  \newfontfamily\titlefontFC{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = Uppercase
  ]
  \usepackage[babel=true]{microtype}
  \usepackage{icomma}

  %% Additionnal colors
  \definecolor{link}{rgb}{0,0.4,0.6}        % internal links
  \definecolor{url}{rgb}{0.6,0,0}           % external links
  \definecolor{codebg}{named}{LightYellow1} % fond code R
  \definecolor{comments}{rgb}{0.5,0.55,0.6} % commentaires
  \definecolor{good}{rgb}{0,0.7,0}          % vert «bien»
  \definecolor{bad}{rgb}{0.7,0,0}           % rouge «mal»
  \definecolor{rouge}{rgb}{0.85,0,0.07}     % UL red stripe
  \definecolor{or}{rgb}{1,0.8,0}            % UL yellow stripe
  \colorlet{alert}{mLightBrown}             % alias Metropolis
  \colorlet{dark}{mDarkTeal}                % alias Metropolis
  \colorlet{code}{mLightGreen}              % alias Metropolis
  \colorlet{shadecolor}{codebg}

  %% Hyperliens
  \hypersetup{%
    pdfauthor = {Vincent Goulet},
    pdftitle = {Programmation lettrée et R~Markdown},
    colorlinks = {true},
    linktocpage = {true},
    urlcolor = {url},
    linkcolor = {link},
    citecolor = {citation},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit}}
  \setlength{\XeTeXLinkMargin}{1pt}

  %% Affichage de la table des matières du PDF
  \usepackage{bookmark}
  \bookmarksetup{%
    open = true,
    depth = 3,
    numbered = true}

  %% Paramétrage de babel pour les guillemets
  \frenchbsetup{og=«, fg=»}

  %% Code source
  \lstloadlanguages{[LaTeX]TeX}
  \lstset{language=[LaTeX]TeX,
    basicstyle=\small\ttfamily\NoAutoSpacing,
    keywordstyle=\mdseries,
    commentstyle=\color{comments},
    morecomment=[s]{```}{```},
    morecomment=[s]{`}{`},
    extendedchars=true,
    showstringspaces=false,
    backgroundcolor=\color{codebg},
    frame=leftline,
    framerule=2pt,
    framesep=5pt,
    xleftmargin=7.4pt
  }

  %% =========================
  %%  Nouveaux environnements
  %% =========================

  %% Environnement pour le code informatique; hybride
  %% des environnements snugshade* et leftbar de framed.
  \makeatletter
  \newenvironment{Scode}{%
    \def\FrameCommand##1{\hskip\@totalleftmargin
      \vrule width 3pt\colorbox{codebg}{\hspace{5pt}##1}%
      % There is no \@totalrightmargin, so:
      \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
    \MakeFramed {\advance\hsize-\width
      \@totalleftmargin\z@ \linewidth\hsize
      \advance\labelsep\fboxsep
      \@setminipage}%
  }{\par\unskip\@minipagefalse\endMakeFramed}
  \makeatother

  %% Environnement pour le contenu d'un fichier; alias de snugshade*.
  \newenvironment{Sfile}{\begin{snugshade*}}{\end{snugshade*}}

  %% =====================
  %%  Nouvelles commandes
  %% =====================

  %% Renvoi vers GitLab sur la page de copyright
  \newcommand{\viewsource}[1]{%
    \href{#1}{\faGitlab\ Voir sur GitLab}}

  %% Renvois vers des fichiers
  \newcommand{\openfile}[1]{%
    \begin{center}
      \colorbox{mDarkTeal}{\color{white}
      \makebox[60mm][c]{%
        \makebox[5mm]{\raisebox{-1pt}{\large\faChevronCircleDown}}\;%
        {\ttfamily #1}}}
    \end{center}}

  %% Identification de la licence CC BY-SA.
  \newcommand{\ccbysa}{\mbox{%
    \faCreativeCommons\kern0.1em%
    \faCreativeCommonsBy\kern0.1em%
    \faCreativeCommonsSa}~\faCopyright[regular]\relax}

  %% Hyperlien avec symbole de lien externe juste après; second
  %% argument peut être vide pour afficher l'url comme lien
  %% [https://tex.stackexchange.com/q/53068/24355 pour procédure de
  %% test du second paramètre vide]
  \usepackage{relsize}
  \newcommand{\link}[2]{%
    \def\param{#2}%
    \ifx\param\empty
      \href{#1}{\nolinkurl{#1}~\raisebox{0.1ex}{\smaller\faExternalLink*}}%
    \else
      \href{#1}{#2~\raisebox{0.1ex}{\smaller\faExternalLink*}}%
    \fi
  }

  %% Noms de fonctions, code, environnement, etc.
  \newcommand{\pkg}[1]{\textbf{#1}}
  \newcommand{\code}[1]{\textcolor{code}{\texttt{#1}}}

  %%% =======
  %%%  Varia
  %%% =======

  %% Longueurs utilisées pour composer les couvertures
  \newlength{\banderougewidth} \newlength{\banderougeheight}
  \newlength{\bandeorwidth}    \newlength{\bandeorheight}
  \newlength{\imageheight}
  \newlength{\logoheight}


\begin{document}

%% frontmatter
\include{couverture-avant}
\include{notices}

\begin{frame}[plain]

  «Finalement, comme plusieurs d'entre-vous n'ont pas beaucoup de
  temps pour [\dots] reprendre tout ce que vous venez de produire en R
  pour en faire un rapport avec de multiples
  copier/coller/effacer/modifier/jeter/recommencer, il serait
  peut-être intéressant pour vous de commencer à utiliser R~Markdown.»

  --- \textbf{Samuel Cabral Cruz}, (alors) analyste en actuariat chez DGAG,
  promotion 2015
\end{frame}

\begin{frame}
  \frametitle{Sommaire}

  \tableofcontents
\end{frame}

%% mainmatter

\section{Concepts généraux}

\begin{frame}
  \frametitle{R Markdown}

  Système de \alert{programmation lettrée} qui permet de combiner le
  code informatique et le texte d'une analyse dans un même fichier.
  \begin{itemize}
  \item Encourage la \alert{science reproductible}
  \item Prise en main rapide et facile grâce à la syntaxe simplifiée
    de \alert{Markdown} pour le texte
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Science (ou recherche) reproductible}

  «Un travail de recherche est dit reproductible si toutes les
  informations qui concernent ce travail, incluant, sans s’y limiter, le
  texte, les données, et le code de programmation, sont rendues
  disponibles de telle sorte que n’importe quel chercheur indépendant
  peut reproduire les résultats.»

  (\href{https://doi.org/10.1109/MSP.2009.932122}{Vandewalle,
    Kovacevic et Vetterli, 2009}; %
  traduction de \href{https://rr-france.github.io/bookrr}{Desquilbet
    et~collab, 2019})
\end{frame}

\begin{frame}
  \frametitle{En termes simples}

  \begin{itemize}
  \item Tout conserver dans un \alert{format} facile à sauvegarder, à
    modifier et à partager
  \item Générer les résultats d'un rapport à partir du \alert{code
      informatique}
  \item Effectuer un \alert{suivi des versions} automatisé de tout ce
    matériel
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Programmation lettrée}

  \begin{minipage}{0.42\linewidth}
    \normalsize
    \begin{quote}
      \selectlanguage{english}
      \raggedright
      I believe that the time is ripe for significantly better
      documentation of programs, and that we can best achieve this by
      considering programs to be works of literature. Hence, my title:
      \emph{Literate Programming}. \par
      \hfill%
      --- Donald Knuth, 1982
    \end{quote}
  \end{minipage}
  \hfill
  \begin{minipage}{0.56\linewidth}
    \includegraphics[width=\linewidth,keepaspectratio]{images/Donald-Knuth_2880_Lede} \\
    \smaller[4]%
    Photo: \href{http://viviancromwell.com/}{Vivian
      Cromwell}. {\textcopyright} Simons Foundation, via
    \href{https://www.quantamagazine.org/computer-scientist-donald-knuth-cant-stop-telling-stories-20200416/}{QuantaMagazine.org}.
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Programmation lettrée}

  Concept de Knuth: composer des programmes comme de la littérature
  afin de les rendre faciles à lire pour un \alert{humain}.
  \begin{itemize}
  \item Combiner la code source et la documentation dans un même fichier
  \item Procédure \emph{weave} pour extraire la documentation
  \item Procédure \emph{tangle} pour extraire le code source
  \item Plusieurs systèmes au fil du temps:
    \link{https://en.wikipedia.org/wiki/WEB}{WEB}~(Knuth, 1984), %
    \link{https://en.wikipedia.org/wiki/CWEB}{CWEB}~(Knuth and Levy, 1987), %
    \link{https://texdoc.org/pkg/doc}{doc}~(Mittelbach, 1989), %
    \link{https://en.wikipedia.org/wiki/Noweb}{noweb}~(Ramsey, 1989), %
    \link{https://en.wikipedia.org/wiki/Sweave}{Sweave}~(Leisch, 2002), %
    \link{https://en.wikipedia.org/wiki/Knitr}{knitr}~(Xie, 2012), %
    \dots
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Illustration}

  Flux de production d'un document de programmation lettrée utilisant
  \alert{\LaTeX} et \alert{R}.

  \begin{center}
    \setlength{\unitlength}{10mm}
    \begin{picture}(11.5,3)(-0.5,0)
      \put(-0.4,1.25){\makebox(0,0){\larger[4]\faFile*[regular]}}
      \put(-0.4,.5){\makebox(0,0){\smaller[1]\code{foo.Rnw}}}
      \put(0.8,1.75){\makebox(0,0){\rotatebox{45}{\larger[3]\faLongArrowAltRight}}}
      \put(0.8,0.75){\makebox(0,0){\rotatebox{-45}{\larger[3]\faLongArrowAltRight}}}

      \put(2,2.5){\makebox(0,0){\larger[4]\faCogs}}
      \put(2,1.75){\makebox(0,0){\smaller[1]\code{Sweave}}}
      \put(2,0){\makebox(0,0){\larger[4]\faCogs}}
      \put(2,-0.75){\makebox(0,0){\smaller[1]\code{Stangle}}}
      \put(3.5,2.5){\makebox(0,0){\larger[3]\faLongArrowAltRight}}
      \put(3.5,0){\makebox(0,0){\larger[3]\faLongArrowAltRight}}

      \put(5,2.5){\makebox(0,0){\larger[4]\faFile*[regular]}}
      \put(5,1.75){\makebox(0,0){\smaller[1]\code{foo.tex}}}
      \put(5,0){\makebox(0,0){\larger[4]\faFile*[regular]}}
      \put(5,-0.75){\makebox(0,0){\smaller[1]\code{foo.R}}}
      \put(6.5,2.5){\makebox(0,0){\larger[3]\faLongArrowAltRight}}

      \put(8,2.5){\makebox(0,0){\larger[4]\faCogs}}
      \put(8,1.75){\makebox(0,0){\smaller[1]\code{latex}}}
      \put(9.5,2.5){\makebox(0,0){\larger[3]\faLongArrowAltRight}}

      \put(11,2.5){\makebox(0,0){\larger[4]\faFilePdf[regular]}}
      \put(11,1.75){\makebox(0,0){\smaller[1]\code{foo.pdf}}}
    \end{picture}
  \end{center}
\end{frame}


\begin{frame}
  \frametitle{Sweave (ou knitr) $+$ \LaTeX}

  Orienté vers la création de documents PDF.
  \begin{itemize}
  \item Texte en format \LaTeX
  \item Code informatique R
  \end{itemize}

  \begin{minipage}[t]{0.48\textwidth}
      \alert{Avantages}
      \begin{itemize}
      \item Qualité typographique, en particulier les équations
        mathématiques
      \item Simplicité de Sweave
      \item Universel et stable
      \item Documentation
      \end{itemize}
  \end{minipage}
  \hfill
  \begin{minipage}[t]{0.48\textwidth}
      \alert{Inconvénients}
      \begin{itemize}
      \item Courbe d'apprentissage
      \item Peu d'intégration avec les outils de partage en ligne
      \end{itemize}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{R~Markdown}

  Orienté vers la création de pages web et de documents PDF.
  \begin{itemize}
  \item Texte en format Markdown
  \item Code informatique R
  \end{itemize}

  \begin{minipage}[t]{0.48\textwidth}
    \alert{Avantages}
    \begin{itemize}
    \item Prise en main rapide et facile grâce à la syntaxe simplifiée
    \item Nombreux formats de sortie
    \item Intégration avec RStudio et GitHub
    \end{itemize}
  \end{minipage}
  \hfill
  \begin{minipage}[t]{0.48\textwidth}
    \alert{Inconvénients}
    \begin{itemize}
    \item Nombreuses couches logicielles (Markdown, YAML, knitr,
      Pandoc, \LaTeX, HTML, \dots)
    \item Documentation
    \item Prise en charge limitée des équations mathématiques
    \item {\LaTeX} requis pour équations complexes ou mises en forme
      particulières
    \end{itemize}
  \end{minipage}
\end{frame}


\section{Markdown}

\begin{frame}
  \frametitle{La puissance du texte brut}

  \begin{minipage}{0.25\linewidth}
    \centering
    \fontsize{100}{100}\selectfont
    \faFile*[regular]
  \end{minipage}
  \hfill
  \begin{minipage}{0.70\linewidth}
    \begin{itemize}
    \item Caractères sans mise en forme
    \item Universel (au type de codage près)
    \item Compatible avec les éditeurs de texte
    \item Pas de surprise, pas de maux de tête
    \end{itemize}
  \end{minipage}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Langage de balisage léger}

  \begin{minipage}{0.50\textwidth}
    \link{https://daringfireball.net/projects/markdown/}{Markdown} est
    un langage de balisage léger pour ajouter de la mise en forme
    \alert{simple} dans un fichier de texte brut.

    \begin{itemize}
    \item Créé par John Gruber en 2004
    \item Dernière mise à jour en 2004 (!)
    \item Syntaxe simple à \alert{lire} et à \alert{écrire} pour
      titres, tableaux, liens, images, etc.
    \item Standard \emph{de facto}
    \end{itemize}
  \end{minipage}
  \hfill
  \begin{minipage}{0.47\textwidth}
\begin{lstlisting}
# Syntaxe Markdown

Markdown permet d'indiquer
de l'*italique* ou du **gras**.

## Listes

- Épicerie
  1. Bananes
  2. Hamburger
	 * pain
	 * viande
- Quincaillerie
\end{lstlisting}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Pandoc}

  Convertisseur universel vers les formats PDF, HTML, DOC, etc.

  \begin{center}
    \includegraphics[angle=90,width=\linewidth,keepaspectratio]{images/pandoc}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{\faCogs\; Exercice}

  Utiliser le fichier \code{exercice-markdown.md}.

  \begin{enumerate}
  \item Ouvrir le fichier dans RStudio.
  \item Copier l'intégralité du texte du fichier et le coller dans
    \link{http://markdownlivepreview.com}{Markdown Live Preview}.
  \item Comparer l'entrée et la sortie.
  \end{enumerate}
\end{frame}


\section{R~Markdown}

\begin{frame}
  \frametitle{Processus de création d'un document en un coup d'oeil}
  \begin{center}
    \setlength{\unitlength}{10mm}
    \begin{picture}(11.5,3)(-1,0)
      \put(-1.2,1.25){\makebox(0,0){\larger[4]\faFile*[regular]}}
      \put(-1.2,0.5){\makebox(0,0){\smaller[1]\code{foo.Rmd}}}
      \put(-0.2,1.25){\makebox(0,0){\larger[3]\faLongArrowAltRight}}

      \put(0.8,1.25){\makebox(0,0){\larger[4]\faCogs}}
      \put(0.8,0.5){\makebox(0,0){\smaller[1]\code{knitr}}}
      \put(1.8,1.25){\makebox(0,0){\larger[3]\faLongArrowAltRight}}

      \put(2.8,1.25){\makebox(0,0){\larger[4]\faFile*[regular]}}
      \put(2.8,.5){\makebox(0,0){\smaller[1]\code{foo.md}}}
      \put(3.8,1.25){\makebox(0,0){\larger[3]\faLongArrowAltRight}}

      \put(4.8,1.25){\makebox(0,0){\larger[4]\faCogs}}
      \put(4.8,0.5){\makebox(0,0){\smaller[1]\code{pandoc}}}
      \put(5.8,1.75){\makebox(0,0){\rotatebox{45}{\larger[3]\faLongArrowAltRight}}}
      \put(5.8,0.75){\makebox(0,0){\rotatebox{-45}{\larger[3]\faLongArrowAltRight}}}

      \put(7,2.5){\makebox(0,0){\larger[4]\faFile*[regular]}}
      \put(7,1.75){\makebox(0,0){\smaller[1]\code{foo.tex}}}
      \put(7,0){\makebox(0,0){\larger[4]\faFileCode[regular]}}
      \put(7,-0.75){\makebox(0,0){\smaller[1]\code{foo.html}}}
      \put(6.4,-1.2){\makebox(0,0)[l]{\smaller[1]\dots\ et divers autres formats de sortie}}
      \put(8,2.5){\makebox(0,0){\larger[3]\faLongArrowAltRight}}
      \put(8,0){\makebox(0,0){\larger[3]\faLongArrowAltRight}}

      \put(9,2.5){\makebox(0,0){\larger[4]\faCogs}}
      \put(9,1.75){\makebox(0,0){\smaller[1]\code{latex}}}
      \put(9,0){\makebox(0,0){\larger[4]\faGlobe}}
      \put(9,-0.75){\makebox(0,0){\smaller[1]Web}}
      \put(10,2.5){\makebox(0,0){\larger[3]\faLongArrowAltRight}}

      \put(11,2.5){\makebox(0,0){\larger[4]\faFilePdf[regular]}}
      \put(11,1.75){\makebox(0,0){\smaller[1]\code{foo.pdf}}}
    \end{picture}
  \end{center}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Exemple de document}

\begin{lstlisting}[title=\code{foo.Rmd}]
L'utilisateur de R interagit avec l'interprète en entrant
des commandes à l'invite de commande:
```{r echo=TRUE}
2 + 3
```
La commande `exp(1)` donne `r exp(1)`, la valeur du nombre $e$.
\end{lstlisting}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Après traitement avec knitr}

\begin{lstlisting}[title=\code{foo.md}]
L'utilisateur de R interagit avec l'interprète en entrant
des commandes à l'invite de commande:

```r
2 + 3
```

```
## [1] 5
```
La commande `exp(1)` donne 2.7182818, la valeur du nombre $e$.
\end{lstlisting}
\end{frame}


\section{Documents simples}

\begin{frame}[fragile=singleslide]
  \frametitle{Anatomie d'un fichier}

  Trois composantes principales d'un fichier R~Markdown:
  \begin{enumerate}
  \item Entête de configuration \link{http://yaml.org}{YAML} (YAML
    Ain't Markup Language)
  \item Texte en format Markdown
  \item Code R sous forme de \\

    \begin{minipage}[t]{0.48\linewidth}
      \alert{blocs de code} (\emph{chunks})
\begin{lstlisting}
```{r}
summary(cars)
```
\end{lstlisting}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{0.5\linewidth}
      \alert{expressions au fil du texte}
\begin{lstlisting}
contient `r nrow(cars)` lignes
et `r ncol(cars)` variables
\end{lstlisting}
    \end{minipage}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{\protect\faCogs\; Exercice}

  Ouvrir le fichier \code{exercice-introduction.Rmd} dans RStudio.

  \begin{enumerate}
  \item Identifier l'entête de configuration dans le fichier.
  \item Identifier un bloc de code R.
  \item Identifier des expressions R au fil du texte.
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Formats de sortie}

  Quelques uns des formats pris en charge par R~Markdown
  (\link{https://rmarkdown.rstudio.com/lesson-9.html}{liste
    complète}).

  \begin{minipage}{0.7\textwidth}
    \begin{itemize}
    \item Document PDF (\code{pdf\_document}; via \LaTeX)
    \item Document HTML (\code{html\_document})
    \item Diapositives avec ioslides (\code{ioslides\_presentation})
    \item Diapositives PDF avec Beamer \\
      (\code{beamer\_presentation}; via \LaTeX)
    \item Word (\code{word\_document})
    \end{itemize}
  \end{minipage}
  \hfill
  \begin{minipage}{0.24\textwidth}
    \includegraphics[width=\linewidth,keepaspectratio]{images/formats}
  \end{minipage}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{\faCogs\; Exercice}

  Utiliser le fichier \code{exercice-introduction.Rmd}.

  \begin{enumerate}
  \item Faire du répertoire dans lequel se trouve le fichier le
    répertoire de travail de R. \\
    \code{Session | Set Working Directory | To Source File Location}
  \item Charger le paquetage \pkg{rmarkdown} dans la session R.
\begin{lstlisting}
> library(rmarkdown)
\end{lstlisting}
  \item Composer le document en format PDF.
\begin{lstlisting}
> render("exercice-introduction.Rmd", encoding = "UTF-8")
\end{lstlisting}
  \item Répéter l'étape précédente après avoir changé
    \code{pdf\_document} dans l'entête pour \code{html\_document}.
  \end{enumerate}
\end{frame}

\begin{frame}[plain]
  \tipbox{Dans RStudio, le bouton
    \raisebox{-0.8ex}{\fboxsep0pt\fbox{\includegraphics[height=3ex,keepaspectratio]{images/knit}}}
    lance directement la composition d'un document.}

  \warningbox{RStudio est livré avec sa propre version de Pandoc et
    c'est celle-ci qui est utilisée lors de toute composition lancée
    depuis l'éditeur. Pour composer dans R sans RStudio, vous devez
    disposer d'une version autonome de Pandoc.}
\end{frame}

\begin{frame}
  \frametitle{Options des blocs de code R}

  Différentes options placées dans \code{`\{r~\}`} permettent de contrôler le
  traitement d'un bloc de code.

  \begin{description}
  \item[\code{echo}] affiche le code source si \code{TRUE} (par
    défaut)
  \item[\code{eval}] exécute le code et affiche la sortie si
    \code{TRUE} (par défaut)
  \item[\code{include}] affiche le contenu du bloc si \code{TRUE} (par
    défaut)
  \end{description}

  \tipbox{Soyez explicite dans les blocs de code, surtout avec
    \code{echo}.}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Utilisation des options}

  \begin{itemize}
  \item Afficher une expression sans l'évaluer ni afficher le résultat
\begin{lstlisting}
```{r echo = TRUE, eval = FALSE}
2 + 3
```
\end{lstlisting}
  \item Afficher un résultat sans montrer l'expression
\begin{lstlisting}
```{r echo = FALSE, eval = TRUE}
2 + 3
```
\end{lstlisting}
  \item Cacher complètement un calcul (mais effectuer ce calcul)
\begin{lstlisting}
```{r include = FALSE}
2 + 3
```
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Configuration du format de sortie}

  Tout passe par l'entête YAML.

  \medskip
  \begin{minipage}{0.48\textwidth}
\begin{lstlisting}
---
title: ""
output:
  html_document:
    toc: TRUE
    toc_float: TRUE
    number_sections: TRUE
    theme: flatly
    highlight: espresso
---
\end{lstlisting}
  \end{minipage}
  \hfill
  \begin{minipage}{0.48\textwidth}
    \importantbox{Dans la syntaxe YAML, l'indentation, la position des
      «\code{:}» et les espaces jouent un rôle.}
  \end{minipage}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{\faCogs\; Exercice}

  Utiliser le fichier \code{exercice-configuration.Rmd}.

  \begin{enumerate}
  \item Composer le fichier tel quel et visualiser la présentation
    ioslides.
  \item Ajouter les options suivantes dans l'entête et visualiser de
    nouveau la présentation:
\begin{lstlisting}
    transition: faster
    widescreen: yes
\end{lstlisting}
  \item Modifier l'entête pour produire plutôt une présentation Beamer
    standard.
\begin{lstlisting}
output:
  beamer_presentation:
\end{lstlisting}
  \item Modifier l'entête pour utiliser les options de Beamer
    suivantes:
\begin{lstlisting}
    latex_engine: xelatex
    theme: metropolis
    highlight: espresso
\end{lstlisting}
  \end{enumerate}
\end{frame}


\section{Mathématiques}

\begin{frame}
  \frametitle{Principes de base}

  \begin{itemize}
  \item Décrire des mathématiques requiert un «langage» spécial
  \item Syntaxe de \LaTeX
  \item Toutes les fonctionnalités de {\LaTeX} ne sont pas disponibles,
    \alert{surtout} en format HTML
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Équations au fil du texte}

  Une équation «en ligne» directement dans le texte comme
  $(a + b)^2 = a^2 + 2ab + b^2$ est placée entre \code{\$~\$}.

\begin{lstlisting}
Une équation «en ligne» directement dans le texte comme
$(a + b)^2 = a^2 + 2ab + b^2$ est placée entre `$ $`.
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Équations hors paragraphe}

  Les équations hors paragraphe séparées du texte principal comme
  \begin{equation*}
    \int_0^\infty f(x) dx =
    \frac{1}{n!}
    \sum_{i = 1}^n \alpha_i e^{x_i} f(x_i)
  \end{equation*}
  sont placées entre \code{\$\$~\$\$}.

\begin{lstlisting}
Les équations hors paragraphe séparées du texte principal comme
$$
\int_0^\infty f(x) dx =
  \frac{1}{n!}
  \sum_{i = 1}^n \alpha_i e^{x_i} f(x_i)
$$
sont placées entre `$$ $$`.
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Sémantique vs apparence}

  Lors de la saisie des équations, veillez à respecter la
  \alert{sémantique} --- ou signification --- du texte plutôt que son
  apparence.
  \begin{itemize}
  \item Saisir les noms de variables en mode mathématique plutôt qu'en
    italique
    \begin{minipage}{0.5\linewidth}
\begin{lstlisting}
Soit *X* la variable aléatoire
\end{lstlisting}
    \end{minipage}\; \raisebox{-0.2ex}{\textcolor{bad}{\faTimesCircle}} \\
    \begin{minipage}{0.5\linewidth}
\begin{lstlisting}
Soit $X$ la variable aléatoire
\end{lstlisting}
    \end{minipage}\; \raisebox{-0.2ex}{\textcolor{good}{\faCheckCircle}}
    \medskip
  \item Saisir l'intégralité de ce qui relève des mathématiques en
    mode mathématique
    \begin{minipage}{0.55\linewidth}
\begin{lstlisting}
Soit $\alpha$ = 3 et $\lambda$ = 1
\end{lstlisting}
    \end{minipage}\; \raisebox{-0.2ex}{\textcolor{bad}{\faTimesCircle}} \\
    \begin{minipage}{0.55\linewidth}
\begin{lstlisting}
Soit $\alpha = 3$ et $\lambda = 1$
\end{lstlisting}
    \end{minipage}\; \raisebox{-0.2ex}{\textcolor{good}{\faCheckCircle}}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Apprendre la syntaxe \LaTeX}

  Consulter le chapitre 7 de
  \link{https://vigou3.gitlab.io/formation-latex-ul}{Rédaction avec
    \LaTeX}.

  Le document étant distribué avec TeX~Live, il se trouve déjà sur
  votre poste de travail.

  Dans RStudio, ouvrez l'onglet Terminal et entrez à la ligne de
  commande du système d'exploitation
\begin{lstlisting}
$ texdoc formation-latex-ul
\end{lstlisting} %$
\end{frame}

\begin{frame}
  \frametitle{\faCogs\; Exercice}

  Reproduire le texte ci-dessous dans les formats PDF et HTML.

  \bigskip
  \fbox{\begin{minipage}{0.9\textwidth}
      La densité conjointe de $X_1$ et $X_2$ est simplement le produit des
      densités marginales:
      \begin{equation*}
        f_{X_1 X_2}(x_1, x_2) = \frac{1}{\Gamma(\alpha)}
        x_2^{\alpha - 1} e^{-(x_1 + x_2)}, \quad
        x_1 > 0, x_2 > 0.
      \end{equation*}
    \end{minipage}}
  \bigskip

  \tipbox{La commande \code{\string\quad} produit un grand espace:
    $|\quad|$.}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{\protect\faCogs\; Exercice synthèse}

  Reproduire avec R~Markdown l'analyse sommaire des données de
  location BIXI~Montréal présentée dans les fichiers
  \code{analyse-bixi.pdf} et \code{analyse-bixi.html}.

  Utiliser l'échantillon des données de BIXI~Montréal dans le fichier
  \code{bixi-sample.rds} livré avec la formation.
\begin{lstlisting}
> bixi <- readRDS("bixi-sample.rds")
\end{lstlisting}
\end{frame}


\section{Dernières observations}

\begin{frame}[plain]
  \begin{center}
    R~Markdown est simple à utiliser, \\
    mais uniquement pour faire des choses simples
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Si vous recherchez plus de contrôle}

  R~Markdown est surtout orienté vers la création de pages web.

  \begin{itemize}
  \item Option pour les plus gros documents:
\link{https://bookdown.org}{Bookdown}
  \item Ajoute une autre (!) couche logicielle par-dessus R~Markdown
  \item Si la mise en forme est importante, je recommande \LaTeX
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Avertissement}

  R~Markdown et Shiny sont des technologies très liées à Posit, la
  compagnie derrière RStudio.

  \begin{itemize}
  \item Créateur de knitr travaille pour Posit
  \item Documentation officielle dans le site de Posit
  \item RStudio incite à utiliser ces formats
  \item Communauté très active dans les réseaux sociaux,
    Stack~Overflow, etc.
  \end{itemize}
\end{frame}


\section{Documentation utile}

\begin{frame}
  \frametitle{Documentation de Markdown}

  \begin{itemize}
  \item \link{https://daringfireball.net/projects/markdown/}{Site officiel}
  \item \link{https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet}{\emph{Cheatsheet}
      de la syntaxe}
  \item \link{http://commonmark.org/help/tutorial/}{Tutoriel
      interactif de 10 minutes}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Documentation de R~Markdown}

  \begin{itemize}
  \item \link{http://rmarkdown.rstudio.com/}{Page d'accueil du projet R~Markdown}
  \item
    \link{https://www.rstudio.com/wp-content/uploads/2015/02/rmarkdown-cheatsheet.pdf}{\emph{Cheatsheet}
      R~Markdown}
  \item
    \link{https://www.rstudio.com/wp-content/uploads/2015/03/rmarkdown-reference.pdf}{Guide
      de référence R~Markdown}
  \item
    \link{http://rmarkdown.rstudio.com/pdf_document_format.html}{Options
      PDF de R~Markdown}
  \item \link{http://kbroman.org/knitr_knutshell/}{Guide knitr}
  \item \link{http://yihui.name/knitr/options/}{Options knitr}
  \end{itemize}
\end{frame}

%% backmatter
\include{colophon}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: t
%%% coding: utf-8
%%% End:
